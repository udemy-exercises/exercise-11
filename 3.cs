using System;

class Student
{
   public static void GetGrades()
   {
      double grades = 0;
      double finalGrades = 0;
      Console.WriteLine("Digite as tres notas do aluno:");

      for (int i = 0; i < 3; i++) {
         grades = double.Parse(Console.ReadLine());
         finalGrades += grades;
      }

      Console.WriteLine($"NOTA FINAL = {finalGrades}");

      if (finalGrades < 60) {
         Console.WriteLine("REPROVADO");
         Console.WriteLine($"FALTARAM {60 - finalGrades}");
      }

      else {
         Console.WriteLine("APROVADO");
      }
   }
}

class Exercise11
{
   static void Main()
   {
      Student.GetGrades();
   }
}
