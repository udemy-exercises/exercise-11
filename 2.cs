using System;

class Exercise11
{
   static void Main()
   {
      var dude = new Employee();

      Console.Write("nome: ");
      dude.name = Console.ReadLine();
      Console.Write("salario: ");
      dude.salary = double.Parse(Console.ReadLine());
      Console.Write("imposto: ");
      dude.taxes = double.Parse(Console.ReadLine());

      dude.salary -= dude.taxes;
      Console.WriteLine(dude);
      dude.salary += dude.taxes;

      Console.Write("digite a porcentagem para aumentar o salario: ");
      int percentSalary = int.Parse(Console.ReadLine());

      dude.salary = (dude.salary - dude.taxes) + (dude.salary / 10);
      Console.WriteLine($"dados atualizados: {dude}");
   }
}

class Employee
{
   public string name;
   public double salary;
   public double taxes;

   public override string ToString()
   {
      return $"Employee: {name}, ${salary}";
   }
}
